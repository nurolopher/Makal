# Makal [![Build Status](https://travis-ci.org/nurolopher/Makal.svg?branch=master)](https://travis-ci.org/nurolopher/Makal) [![Coverage Status](https://coveralls.io/repos/nurolopher/Makal/badge.svg?branch=master&service=github)](https://coveralls.io/github/nurolopher/Makal?branch=master)
Kyrgyz Proverbs.

This is a source code of Kyrgyz Proverbs Android application.

## Download
In order to download the application go to [Kyrgyz Proverbs](https://play.google.com/store/apps/details?id=com.nurolopher.makal) Google Play Store
