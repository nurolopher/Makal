package com.nurolopher.makal;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Nursultan Turdaliev on 12/13/15.
 */
public class StatusUnitTest {

    @Test
    public void testConstants() {

        assertEquals(Status.STATUS_FAVORITE, "FAVORITE");
        assertEquals(Status.STATUS_NEW, "NEW");

    }

}