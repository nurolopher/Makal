package com.nurolopher.makal;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Nursultan Turdaliev on 12/13/15.
 */
public class ProverbTest {

    @Test
    public void testGetFavouriteIconResId() throws Exception {

        Proverb proverb = new Proverb();

        proverb.setStatus(Status.STATUS_NEW);
        assertEquals(proverb.getFavouriteIconResId(), R.drawable.ic_favorite_outline_24dp);

        proverb.setStatus(Status.STATUS_FAVORITE);
        assertEquals(proverb.getFavouriteIconResId(),R.drawable.ic_favorite_24dp);
    }

    @Test
    public void testGetFirstAlphabet() throws Exception {

        Proverb proverb = new Proverb();

        proverb.setTitle("Nursultan");
        assertEquals(proverb.getFirstAlphabet(), "N");

        proverb.setTitle("  Alma");
        assertEquals(proverb.getFirstAlphabet(),"A");


    }
}