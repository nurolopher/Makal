package com.nurolopher.makal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private static final String LAUNCHED = "LAUNCHED";

    private int index = 1;

    private Realm realm;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configTabs();
        init();
        configFragments();
        configTabLayout();

        loadFixtures();
    }

    private void init() {
        realm = Realm.getInstance(this);
    }

    private void configTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void configTabs() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void configFragments() {
        /*
      The {@link android.support.v4.view.PagerAdapter} that will provide
      fragments for each of the sections. We use a
      {@link FragmentPagerAdapter} derivative, which will keep every
      loaded fragment in memory. If this becomes too memory intensive, it
      may be best to switch to a
      {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFragment(ListFragment.newInstance(realm, this), getResources().getString(R.string.tab_title_list));
        mSectionsPagerAdapter.addFragment(FavouriteFragment.newInstance(realm, this), getResources().getString(R.string.tab_title_group));

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    private void loadFixtures() {
        if (!isLaunched()) {
            new LoadFixturesTask().execute("");
        }
    }

    private boolean isLaunched() {
        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        boolean launched = sharedPreferences.getBoolean(MainActivity.LAUNCHED, false);
        if(!launched){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(MainActivity.LAUNCHED,true);
            editor.apply();
        }
        return launched;
    }

    private class LoadFixturesTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Realm realm;
            try {


                //RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).build();
                //Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(getApplicationContext());
                InputStream inputStream = getAssets().open("fixtures/1.txt");
                final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));


                String line;
                while ((line = reader.readLine()) != null) {

                    realm.beginTransaction();
                    Proverb proverb = realm.createObject(Proverb.class);
                    proverb.setId(index++);
                    proverb.setTitle(line);
                    proverb.setStatus(com.nurolopher.makal.Status.STATUS_NEW);
                    realm.commitTransaction();
                }
            } catch (IOException e) {
                Log.e(MainActivity.class.toString(), "Fixture: " + e.getMessage());
                return 0;
            }
            realm.close();
            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }
    }


}
