package com.nurolopher.makal;

/**
 * Created by Nursultan Turdaliev on 12/12/15.
 */
public class Status {

    public static final String STATUS_NEW = "NEW";
    public static final String STATUS_FAVORITE = "FAVORITE";
}
