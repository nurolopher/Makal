package com.nurolopher.makal;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nursultan Turdaliev on 12/12/15.
 */
public class ProverbAdapter extends RecyclerView.Adapter<ProverbViewHolder> {

    private final Context mContext;
    private RealmResults<Proverb> realmResults;
    private String status;

    public ProverbAdapter(RealmResults<Proverb> realmResults, Context context,String status) {
        this.realmResults = realmResults;
        this.mContext = context;
        this.status = status;
    }

    @Override
    public ProverbViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.proverb_list_row, parent, false);
        return new ProverbViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProverbViewHolder holder, int position) {
        holder.bindProverb(realmResults.get(position));
        handleClicks(holder, position);
    }

    private void handleClicks(final ProverbViewHolder viewHolder, final int position) {
        ImageButton imageButton = viewHolder.mImageButton;
        imageButton.setOnClickListener(new View.OnClickListener() {
            Proverb proverb = realmResults.get(position);

            @Override
            public void onClick(View v) {
                Realm realm = Realm.getInstance(mContext);
                realm.beginTransaction();
                toggleStatus(proverb);
                realm.commitTransaction();

                realmResults = realm.where(Proverb.class).equalTo("status", status).findAll();
                //setRealmResults(realmResults);
                notifyDataSetChanged();
                if(status.equalsIgnoreCase(Status.STATUS_NEW)) {
                    showRestoreSnackBar(viewHolder, proverb);
                }
            }

        });
    }

    private void toggleStatus(Proverb proverb) {
        if (proverb.getStatus().equalsIgnoreCase(Status.STATUS_NEW)) {
            proverb.setStatus(Status.STATUS_FAVORITE);
        }else {
            proverb.setStatus(Status.STATUS_NEW);
        }
    }

    private void showRestoreSnackBar(ProverbViewHolder viewHolder, final Proverb proverb) {
        Snackbar.make(viewHolder.mTitle.getRootView().findViewById(R.id.listFragmentRoot), R.string.proverb_restore_message, Snackbar.LENGTH_LONG)
                .setAction(R.string.restore, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Realm realm = Realm.getInstance(mContext);
                        realm.beginTransaction();
                        proverb.setStatus(Status.STATUS_NEW);
                        realm.commitTransaction();
                        realmResults = realm.where(Proverb.class).equalTo("status", status).findAll();
                        notifyDataSetChanged();
                    }
                }).show();
    }

    @Override
    public int getItemCount() {
        return realmResults.size();
    }

    public void setRealmResults(RealmResults<Proverb> realmResults) {
        this.realmResults = realmResults;
    }
}
