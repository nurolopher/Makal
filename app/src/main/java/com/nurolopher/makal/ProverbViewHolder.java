package com.nurolopher.makal;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Nursultan Turdaliev on 12/12/15.
 */
public class ProverbViewHolder extends RecyclerView.ViewHolder {

    public TextView mTitle;
    public ImageButton mImageButton;
    private TextView mAlphabet;

    public ProverbViewHolder(View itemView) {
        super(itemView);
        mTitle = (TextView)itemView.findViewById(R.id.proverb_title);
        mImageButton = (ImageButton)itemView.findViewById(R.id.imageButton);
        mAlphabet = (TextView)itemView.findViewById(R.id.alphabet);
    }

    public void bindProverb(Proverb proverb){
        mTitle.setText(proverb.getTitle());
        mImageButton.setBackgroundResource(proverb.getFavouriteIconResId());
        mAlphabet.setText(proverb.getFirstAlphabet());
    }
}
