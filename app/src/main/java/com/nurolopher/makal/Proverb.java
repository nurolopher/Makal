package com.nurolopher.makal;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Nursultan Turdaliev on 12/10/15.
 */
public class Proverb extends RealmObject {


    @PrimaryKey
    private int id;

    @Index
    @Required
    private String title;

    @Ignore
    private int favouriteIconResId = 0;

    @Ignore
    private String firstAlphabet;

    @Required
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getFavouriteIconResId() {
        if (getStatus() == null) {
            favouriteIconResId = R.drawable.ic_favorite_24dp;
        } else if (getStatus().equalsIgnoreCase(Status.STATUS_NEW)) {
            favouriteIconResId = R.drawable.ic_favorite_outline_24dp;
        } else if (getStatus().equalsIgnoreCase(Status.STATUS_FAVORITE)) {
            favouriteIconResId = R.drawable.ic_favorite_24dp;
        }
        return favouriteIconResId;
    }

    public String getFirstAlphabet() {
        firstAlphabet = String.valueOf(getTitle().trim().charAt(0));
        return firstAlphabet;
    }


}
